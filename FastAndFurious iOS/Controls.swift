//
//  GameController.swift
//  FastAndFurious iOS
//
//  Created by Abhishek Shinde on 05/06/18.
//  Copyright © 2018 Abhishek Shinde. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

enum NotificationNames:String {
    case left = "LeftTurn"
    case right = "RightTurn"
    case accelerate = "Accelerate"
    case brake = "Brake"
}



class Controls: SKScene {

    override func didMove(to view: SKView) {
        print("did move")
        // Get label node from scene and store it for use later

    }
    
  
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch:UITouch = touches.first{
            let pos = touch.location(in: self)
            let touchedNode = self.nodes(at: pos).first
            
            print(touchedNode?.name ?? "b;a")
            
            switch touchedNode?.name{
            case NotificationNames.left.rawValue?:
                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationNames.left.rawValue), object: nil)
                
            case NotificationNames.right.rawValue?:
                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationNames.right.rawValue), object: nil)

            case NotificationNames.accelerate.rawValue?:
                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationNames.accelerate.rawValue), object: nil)

            case NotificationNames.brake.rawValue?:
                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationNames.brake.rawValue), object: nil)

            default:break
            }

            
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
