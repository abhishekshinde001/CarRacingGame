//
//  GameViewController.swift
//  FastAndFurious iOS
//
//  Created by Abhishek Shinde on 23/05/18.
//  Copyright © 2018 Abhishek Shinde. All rights reserved.
//

import UIKit
import SceneKit
import SpriteKit

class GameViewController: UIViewController {
    var carNode:SCNNode?
    var gameView: SCNView {
        return self.view as! SCNView
    }
    
    var gameController: GameController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gameController = GameController(sceneRenderer: gameView)
        
        // Allow the user to manipulate the camera
        self.gameView.allowsCameraControl = false
        
        // Show statistics such as fps and timing information
        self.gameView.showsStatistics = true
        
        // Configure the view
        self.gameView.backgroundColor = UIColor.black
        
        // Add a tap gesture recognizer
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
//        var gestureRecognizers = gameView.gestureRecognizers ?? []
//        gestureRecognizers.insert(tapGesture, at: 0)
//        self.gameView.gestureRecognizers = gestureRecognizers
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(_ :)), name: NSNotification.Name(rawValue: NotificationNames.left.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(_ :)), name: NSNotification.Name(rawValue: NotificationNames.right.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(_ :)), name: NSNotification.Name(rawValue: NotificationNames.brake.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(_ :)), name: NSNotification.Name(rawValue: NotificationNames.accelerate.rawValue), object: nil)
        
        
        carNode = self.gameController.scene.rootNode.childNode(withName: "CAR", recursively: true)
        
        let overlayScene = SKScene(fileNamed: "Controls.sks")

        if let _ = overlayScene?.childNode(withName: "Accelerate"){
            self.gameView.overlaySKScene = overlayScene
        }

    }

    
    
    @objc func handleNotification(_ notification:Notification){
        print(notification.name)
        
        switch notification.name.rawValue {
            
        case NotificationNames.accelerate.rawValue:
            carNode?.runAction(SCNAction.moveBy(x: 0, y: -200, z: 0, duration: 1))
            
        case NotificationNames.brake.rawValue:
            carNode?.removeAllActions()
            
        case NotificationNames.left.rawValue:
            self.carNode?.runAction(SCNAction.rotateBy(x: 0, y: 0, z: 0.2, duration: 1))
        
        case NotificationNames.right.rawValue:
            self.carNode?.runAction(SCNAction.rotateBy(x: 0, y: 0, z: -0.2, duration: 1))
        default:
            break
        }
    }
    
    @objc
    func handleTap(_ gestureRecognizer: UIGestureRecognizer) {        
        // Highlight the clicked nodes
        let p = gestureRecognizer.location(in: gameView)
        gameController.highlightNodes(atPoint: p)

        carNode?.runAction(SCNAction.moveBy(x: 0, y: -100, z: 0, duration: 1),completionHandler:{
                self.carNode?.runAction(SCNAction.rotateBy(x: 0, y: 0, z: 3.1, duration: 1),completionHandler:{
                    self.carNode?.runAction(SCNAction.moveBy(x: 0, y: 10000, z: 0, duration: 3
                        ),completionHandler:{
                            self.carNode?.runAction(SCNAction.rotateBy(x: 0, y: 0, z: 3.1, duration: 1))
                    })
                })
        })
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
