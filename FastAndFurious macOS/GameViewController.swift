//
//  GameViewController.swift
//  FastAndFurious macOS
//
//  Created by Abhishek Shinde on 23/05/18.
//  Copyright © 2018 Abhishek Shinde. All rights reserved.
//

import Cocoa
import SceneKit
import SpriteKit

class GameViewController: NSViewController {
    
    var carNode:SCNNode?
    var gameView: SCNView {
        return self.view as! SCNView
    }
    
    var gameController: GameController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gameController = GameController(sceneRenderer: gameView)
        
        // Allow the user to manipulate the camera
        self.gameView.allowsCameraControl = false
        
        // Show statistics such as fps and timing information
        self.gameView.showsStatistics = true
        
        // Configure the view
        self.gameView.backgroundColor = NSColor.black
        
        // Add a click gesture recognizer
        let clickGesture = NSClickGestureRecognizer(target: self, action: #selector(handleClick(_:)))
        var gestureRecognizers = gameView.gestureRecognizers
        gestureRecognizers.insert(clickGesture, at: 0)
        self.gameView.gestureRecognizers = gestureRecognizers
        
        carNode = self.gameController.scene.rootNode.childNode(withName: "CAR", recursively: true)
        //Controls
        
        var overlayScene = SKScene(fileNamed: "GameController.sks")
        
        if let _ = overlayScene?.childNode(withName: "Controls"){
            self.gameView.overlaySKScene = overlayScene
        //    overlayScene?.position.x = (self.gameView.scene?.rootNode.position.x)!
         //   overlayScene?.position.y = (self.gameView.scene?.rootNode.position.y)!
            
        }
    }
    
    @objc
    func handleClick(_ gestureRecognizer: NSGestureRecognizer) {
        // Highlight the clicked nodes
        let p = gestureRecognizer.location(in: gameView)
        gameController.highlightNodes(atPoint: p)
        
        
        carNode?.runAction(SCNAction.moveBy(x: 0, y: -10000, z: 0, duration: 3
        ),completionHandler:{
            self.carNode?.runAction(SCNAction.rotateBy(x: 0, y: 0, z: 3.1, duration: 1),completionHandler:{
                self.carNode?.runAction(SCNAction.moveBy(x: 0, y: 10000, z: 0, duration: 3
                ),completionHandler:{
                    self.carNode?.runAction(SCNAction.rotateBy(x: 0, y: 0, z: 3.1, duration: 1))
                })
            })
        })
    
    }
    
    
    
}
